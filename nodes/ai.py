#!/usr/bin/env python
from __future__ import division

import rospy
import math
import time
import cv2
import numpy as np
import random

from pybricks.rosrider import ROSRider
from pybricks.rosrider import Vector3D

# will move later but make sure this loads after robot -
ai = ROSRider('robot2', 2)
time.sleep(0.5)

def is_point_in_wall(wall, x, y, margin):
    if x > wall.x1 - margin and x < wall.x2 + margin:
        if y > wall.y1 - margin and y < wall.y2 + margin:
            return True
    return False

# update this once each cycle to reduce load on ros
my_position = Vector3D(0,0,0)

def drive_to(tx, ty):

    # get my current position
    p = my_position

    # we need a buffer so we don't clip the edge of the wall so we give a margin
    margin = 0.25

    # make a sub-divided map
    sub_divisions = 5 # each unit 1x1 will be 5x5 of size 0.2x0.2
    world_width = 6
    N = world_width * sub_divisions
    sub_divisions_size = 1.0/float(sub_divisions)

    # maybe store later to avoid inefficiency of mem allocation - but probably not very important
    grid = [0] * N * N

    origin_x = -3.0
    origin_y = -3.0

    # now scan the grid - set the walls to 0 if they are in a wall
    # TODO: Move this to a stored spot - we only need to calculate it once
    for i in range(N):
        for j in range(N):
            for wall in ai.interior_walls:
                x = (float(i) + 0.5) * sub_divisions_size
                y = (float(j) + 0.5) * sub_divisions_size
                if is_point_in_wall(wall, x + origin_x, y + origin_y, margin):
                    grid[i+j*N] = -1

    # start flood fill at the target point
    ti = int((tx - origin_x)/sub_divisions_size)
    tj = int((ty - origin_y)/sub_divisions_size)

    # clamp it since user may hit us with bad points
    if ti < 0:
        ti = 0
    if tj < 0:
        tj = 0
    if ti >= N:
        ti = N - 1
    if tj >= N:
        tj = N - 1

    # user position point
    si = int((p.x - origin_x)/sub_divisions_size)
    sj = int((p.y - origin_y)/sub_divisions_size)

    flood_fill = 100
    grid[ti+tj*N] = flood_fill
    bDone = False
    while not bDone:
        for i in range(N):
            for j in range(N):
                if grid[i+j*N] == flood_fill:
                    for i2 in range(i-1, i+2):
                        for j2 in range(j-1, j+2):
                            if i == i2 or j == j2: # don't flood fill diagonal
                                if i2 >= 0 and i2 < N and j2 >= 0 and j2 < N:
                                    # allow last link to flood fill into a wall blocked cell
                                    # if we are right against wall we could be in blocked
                                    if grid[i2+j2*N] == 0 or (i2 == si and j2 == sj):
                                        grid[i2+j2*N] = flood_fill + 1
                                        if i2 == si and j2 == sj: # did we arrive at player position
                                            bDone = True
        flood_fill += 1

        if flood_fill > 199:
            print 'Bad flood fill'
            break

    #  if self.index == 2:
    #      print 'Grid Init'
    #      for j in range(N):
    #          for i in range(N):
    #              v = grid[i+j*N]
    #              if v == -1:
    #                  print(' X '),
    #              elif v == 0:
    #                  print(' 0 '),
    #              else:
    #                  print(v),
    #          print ' '

    # check if walls are close to our path
    # if not we can just head straight to the target
    # this fixes the robot being slightly off as it gets closer
    mi = si
    mj = sj
    near_walls = False
    counter = 0
    while not near_walls and (mi != ti or mj != tj):
        si = mi
        sj = mj
        for si2 in range(si - 1, si + 2):
            for sj2 in range(sj - 1, sj + 2):
                if si2 >= 0 and si2 < N and sj2 >= 0 and sj2 < N:
                    if grid[si2+N*sj2] == grid[si+N*sj] - 1:
                        mi = si2
                        mj = sj2
        # purpose of above loop separate from this loop is to check first if we touched then final point
        # then skip the wall check
        # this is so if the opponent is againt the wall but we have line of sight,
        # we can still consider the path not near_walls and angle directly to the opponent
        if mi != ti or mj != tj:
            for si2 in range(si - 1, si + 2):
                for sj2 in range(sj - 1, sj + 2):
                    if si2 >= 0 and si2 < N and sj2 >= 0 and sj2 < N:
                        if grid[si2+N*sj2] == -1:
                            near_walls = True

        counter += 1
        if counter > 100:
            print 'Bad search walls', 'counter:', counter, 'mij:', mi, mj
            break
    
    # print 'Did find target after', counter, "Near Walls:", near_walls, self.index

    if not near_walls: # just drive to target
        return math.atan2(ty - p.y, tx - p.x)
    
    # reset end point
    #TODO: Fix flow - should not have overwritten this above - keep it fixed
    si = int((p.x - origin_x)/sub_divisions_size)
    sj = int((p.y - origin_y)/sub_divisions_size)

    # we have walls near our path so let's just move to the next best cell
    # search for neighbor point which is one less than our point
    #best_dist_sq = 9999999.9
    best_nx = 0.0
    best_ny = 0.0
    counter = 0
    for si2 in range(si - 1, si + 2):
        for sj2 in range(sj - 1, sj + 2):
            if si2 >= 0 and si2 < N and sj2 >= 0 and sj2 < N:
                if grid[si2+N*sj2] == grid[si+N*sj] - 1:
                    best_nx += si2
                    best_ny += sj2
                    counter += 1
    best_nx/=counter
    best_ny/=counter

    return math.atan2(best_ny - sj, best_nx - si) # to end point

class Info:
    def __init__(self, robot, default_delay):
        self.robot = robot
        self.counter = 0
        self.escape_counter = 0
        self.escape_x = 0
        self.escape_y = 0
        self.default_delay = default_delay
        self.filter_move_distance = 0
        self.last_position = Vector3D(0,0,0)
        self.run_around_timer = 0
        self.last_escape_x = 0
        self.last_escape_y = 0

ai_info = Info(ai, 15)

def get_delta_angle(current_angle, target_angle):
    delta_angle = target_angle - current_angle
    while delta_angle > math.pi:
        delta_angle -= math.pi * 2.0
    while delta_angle < -math.pi:
        delta_angle += math.pi * 2.0
    return delta_angle

def strategy0(info):
    # dumb robot that always drives to opponent and fires
    # AI should easily defeat this robot to be considered legitimate
    r = info.robot
    delta = r.opponent_position - r.position
    distance = delta.length()
    angle_to_opp = math.atan2(delta.y, delta.x)
    delta_angle_to_opponent = get_delta_angle(r.angle, angle_to_opp)
    delta_angle = get_delta_angle(r.angle, drive_to(r.opponent_position.x, r.opponent_position.y))
    if distance < 2.0 and r.can_fire() and abs(delta_angle_to_opponent) < 0.25:
        r.fire()
    r.rotate(delta_angle * 2.0)
    if r.front_laser_data() < 0.3 and abs(delta_angle) > 0.1:
        r.move(0.0)
    else:
        r.move(1.0)

def strategy1(info):
    r = info.robot
 
    info.run_around_timer -= 1
    if info.run_around_timer < -100:
        info.run_around_timer = 100

    moved = (r.position - info.last_position).length()
    low_pass_filter = 0.05
    info.filter_move_distance += (moved - info.filter_move_distance) * low_pass_filter
    info.last_position = r.position
    delta = r.opponent_position - r.position
    distance = delta.length()
    angle_to_opp = math.atan2(delta.y, delta.x)
    opp_angle_to_me = angle_to_opp + math.pi


    user_front_distance = r.front
    w = 3.0

    # save this separate to decide about firing
    delta_angle_to_opponent = get_delta_angle(r.angle, angle_to_opp)
    delta_opp_angle_to_me = get_delta_angle(r.opponent_angle, opp_angle_to_me)

    if r.can_fire() and distance < 0.3:
        r.move(0.0) # try to back up
        r.rotate(delta_angle_to_opponent * 4.0)
        if abs(delta_angle_to_opponent) < 0.2:
            r.fire()
        return

    close_enough_path_done = 1.0
    # now get pathing angle
    # this is the angle the pathing algorithm says we should turn to for getting around walls
    # so we can arrive at the opponent position
    if info.escape_counter > 0:
        delta_angle = get_delta_angle(r.angle, drive_to(info.escape_x, info.escape_y))
        dx = info.escape_x - r.position.x
        dy = info.escape_y - r.position.y
        escape_dist_sq = (dx*dx + dy*dy)
        if escape_dist_sq < close_enough_path_done * close_enough_path_done:
            info.escape_counter = 0 # done with this escape path
            info.last_escape_x = info.escape_x
            info.last_escape_y = info.escape_y
    else:
        delta_angle = get_delta_angle(r.angle, drive_to(r.opponent_position.x, r.opponent_position.y))

    move_direction = 1.0
    if not r.can_fire() or distance < 0.5 or abs(delta_opp_angle_to_me) < 0.05 or info.run_around_timer < 0:
        # get info about best corners
        search_dist = close_enough_path_done
        if distance > 1.0 and info.run_around_timer < 0:
            search_dist = 3.0 # find new spots
        escape_margin = 0.5
        best_x = 0
        best_y = 0
        best_distance = 0
        best_x2 = 0
        best_y2 = 0
        best_distance2 = 0
        for corner in range(0, 4):
            x = 0
            y = 0
            if corner == 0:
                x = -w + escape_margin
                y = -w + escape_margin
            if corner == 1:
                x = w - escape_margin
                y = -w + escape_margin
            if corner == 2:
                x = w - escape_margin
                y = w - escape_margin
            if corner == 3:
                x = -w + escape_margin
                y = w - escape_margin
            dx = x - r.position.x
            dy = y - r.position.y

            ldx = info.last_escape_x - x
            ldy = info.last_escape_y - y
            dist_last_sq = ldx*ldx + ldy*ldy
            dist = math.sqrt(dx*dx + dy*dy)
            dotp = (dx * delta.x + dy * delta.y)/(distance * dist)

            if dist_last_sq > search_dist * search_dist * 1.1:
                if dist > best_distance2:
                    if dotp < 0.6: # is pointing away from opponent?
                        best_distance2 = dist
                        best_x2 = x
                        best_y2 = y
                if dist > best_distance:
                    if dotp < 0.0: # is pointing away from opponent?
                        best_distance = dist
                        best_x = x
                        best_y = y
 
        # evasive backwards only if we are not close to the edge
        roam_escape = False

        if info.escape_counter <= 0 and (r.position.x < -w+escape_margin or r.position.y < -w+escape_margin or r.position.x > w-escape_margin or r.position.y > w-escape_margin):
            roam_escape = True
        if (info.escape_counter <= 0 or distance < 1.0) and info.run_around_timer < 0:
            roam_escape = True

        if roam_escape:
            # escape to the corner which is away from us and not in path of opponent
            if best_distance > close_enough_path_done:
                info.escape_x = best_x
                info.escape_y = best_y
                info.escape_counter = 15 # we will run to another spot
            elif best_distance2 > close_enough_path_done:
                info.escape_x = best_x2
                info.escape_y = best_y2
                info.escape_counter = 15 # we will run to another spot

        # if not escaping and sort of pointing at opponent do reverse evasive actions
        # note we do not want to back up if opponent is behind us ever
        # but we may want to reverse if close and parallel - to avoid a circling behavior
        if distance < 1.0 or info.run_around_timer >= 0:
            if info.escape_counter <= 0 and (abs(delta_angle_to_opponent) < 0.5 or (distance < 1.0 and abs(delta_angle_to_opponent) < 2.0)):
                if best_distance > close_enough_path_done:
                    reverse_x = best_x
                    reverse_y = best_y
                elif best_distance2 > close_enough_path_done:
                    reverse_x = best_x2
                    reverse_y = best_y2
                else:
                    reverse_x = 0 # try origin
                    reverse_y = 0
                delta_angle = get_delta_angle(r.angle, drive_to(reverse_x, reverse_y))
                delta_angle += 0.02 * (random.random() - 0.5) # with some random evasion
                delta_angle += math.pi # flip angle for drive backwards
                move_direction = -1.0 # drive backwards
                while delta_angle > math.pi:
                    delta_angle -= math.pi * 2.0
                while delta_angle < -math.pi:
                    delta_angle += math.pi * 2.0


    
    if distance < 1.0:
        info.counter = info.default_delay
    if distance < 2.0 and abs(delta_angle_to_opponent) < 0.25 and r.can_fire():
        r.fire()

    
    if user_front_distance < 0.3 and abs(delta_angle) > 0.1:
        r.move(0)
        r.rotate(delta_angle * 3.0)
    elif abs(delta_angle) > 0.2 and info.filter_move_distance < 0.01:
        r.move(0)
        r.rotate(delta_angle * 3.0)
    else:
        r.rotate(delta_angle * 1.0)
        if move_direction < 0:
            r.move(-1.0) # speed back
        else:
            working_speed = 1.0
            if r.can_fire():
                working_speed = 1.0
            r.move(move_direction * max(0.1, working_speed - abs(delta_angle * 0.25)))

          #  print 'moving front', user_front_distance

    info.counter -= 1
    info.escape_counter -= 1

#Your code starts here. Use while.robot.is_ok() as your loop statement:
while(ai.is_ok()):
    my_position = ai.position
    strategy1(ai_info)
