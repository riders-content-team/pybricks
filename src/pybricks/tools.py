#!/usr/bin/env python
import time

def wait(duration):
    time.sleep(float(duration)/1000)