#!/usr/bin/env python

class Port:
    A = 0
    B = 1
    C = 2
    S4 = 4


class Stop:
    BRAKE = 0


class Color:
    BLACK = (0, 0, 0)
    WHITE = (255, 255, 255)