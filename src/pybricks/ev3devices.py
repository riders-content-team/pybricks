#!/usr/bin/env python
import rospy
from rosrider_description.srv import CameraSensor
from pybricks.parameters import (Color)
import math

COLORS = (
    (0, 0, 0),
    (255, 255, 255),
)

class Motor:
    def __init__(self, port):
        self.port = port


class ColorSensor:
    def __init__(self, port):
        self.port = port

        self._init_sensor_services()

    def _init_sensor_services (self):
        try:
            rospy.wait_for_service("/rosrider_camera_1/rosrider_get_image", 0.5)

            self.image_data_service = rospy.ServiceProxy('/rosrider_camera_1/rosrider_get_image', CameraSensor)

            resp = self.image_data_service()


            #self.image.convert_str_to_rgb(resp.data)

            return True
        except (rospy.ServiceException, rospy.ROSException), e:
            rospy.logerr("Service call failed: %s" % (e,))

            return False

    def color(self):
        resp = self.image_data_service()
        
        return self.__closest_color(
            ord(resp.data[0]), 
            ord(resp.data[1]), 
            ord(resp.data[2]),
        )


    def __closest_color(self, r, g, b):
        color_diffs = []
        for color in COLORS:
            cr, cg, cb = color
            color_diff = math.sqrt((r - cr)**2 + (g - cg)**2 + (b - cb)**2)
            color_diffs.append((color_diff, color))
        return min(color_diffs)[1]
