#!/usr/bin/env python

import rospy
import math

from std_msgs.msg import String
from geometry_msgs.msg import Twist
from sensor_msgs.msg import Image
from std_srvs.srv import Empty
from nav_msgs.msg import Odometry
from rosrider_description.srv import CameraSensor, RosriderControl

from tf.transformations import quaternion_from_euler, euler_from_quaternion


class ROSRider:

    class _Image:
        def __init__(self):
            self.height = 0
            self.width = 0
            self.data = []

        def convert_str_to_rgb(self, data):
            self.data = []

            for i in range(self.height * self.width * 3):
                self.data.append(ord(data[i]))

    # init method. image_cb is camera callback function
    def __init__(self, robotName):
        rospy.init_node("rosrider_controller")

        self.image = self._Image()
        self.is_shutdown = False

        self._init_robot_control()
        self._init_sensor_services()

        self.linear_speed = 0
        self.angular_speed = 0
        self.step_size = 10

    def _init_robot_control(self):
        try:
            rospy.wait_for_service("/rosrider_control1", 1)

            self.control = rospy.ServiceProxy('/rosrider_control1', RosriderControl)
            return True

        except (rospy.ServiceException, rospy.ROSException), e:
            rospy.logerr("Service call failed: %s" % (e,))
            print("No sensor")
            return False

    def _init_sensor_services (self):
        try:
            rospy.wait_for_service("/rosrider_camera_1/rosrider_get_image", 0.5)

            self.image_data_service = rospy.ServiceProxy('/rosrider_camera_1/rosrider_get_image', CameraSensor)

            resp = self.image_data_service()

            self.image.height = resp.height
            self.image.width = resp.width

            self.image.convert_str_to_rgb(resp.data)

            return True

        except (rospy.ServiceException, rospy.ROSException), e:
            rospy.logerr("Service call failed: %s" % (e,))

            return False

    def _check_game_controller(self):
        rospy.wait_for_message('/simulation_metrics', String, timeout=5)

    def move(self, linear_speed):
        self.linear_speed = linear_speed
        if self.control:
            self.control(self.linear_speed, self.angular_speed, self.step_size)
        else:
            return

    def rotate(self, angular_speed):
        self.angular_speed = angular_speed
        if self.control:
            self.control(self.linear_speed, self.angular_speed, self.step_size)
        else:
            return

    def get_sensor_data(self):
        image = self.image_data()
        sensor_row = []
        for i in range(image.width):
            brightness = (0.2126 * ord(image.data[i * 3])) + (0.7152 * ord(image.data[i * 3 + 1])) + (0.0722 * ord(image.data[i * 3 + 2]))
            sensor_row.append(brightness)
        return sensor_row

    def image_data(self):
        resp = self.image_data_service()

        self.image.data = resp.data

        return self.image


    def is_ok(self):
        if not rospy.is_shutdown():
            self.image_data()
            return True
        else:
            return False
