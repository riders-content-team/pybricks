#!/usr/bin/env python

import rospy
import math

from std_msgs.msg import String
from geometry_msgs.msg import Twist
from sensor_msgs.msg import Image
from std_srvs.srv import Empty
from nav_msgs.msg import Odometry
from rosrider_description.srv import CameraSensor, RosriderControl

from tf.transformations import quaternion_from_euler, euler_from_quaternion
import time

class DriveBase:
    # init method. image_cb is camera callback function
    def __init__(self, motor_1, motor_2, id_1, id_2):
        rospy.init_node("rosrider_controller")

        self.is_shutdown = False

        self._init_robot_control()

        self.step_size = 10

        time.sleep(1)

    def _init_robot_control(self):
        try:
            rospy.wait_for_service("/rosrider_control1", 1)

            self.control = rospy.ServiceProxy('/rosrider_control1', RosriderControl)
            return True

        except (rospy.ServiceException, rospy.ROSException), e:
            rospy.logerr("Service call failed: %s" % (e,))
            print("No sensor")
            return False

    def drive_time(self, linear_speed, angular_speed, duration):
        lx = float(linear_speed)/1000
        az = float(angular_speed)/1000
        self.control(lx, az, self.step_size)

        time.sleep(float(duration)/1000)
        self.control(0, 0, self.step_size)
    
    def drive(self, linear_speed, angular_speed):
        lx = float(linear_speed)/1000
        az = float(angular_speed)/1000
        self.control(lx, az, self.step_size)

    def stop(self, stop_type):
        self.control(0, 0, self.step_size)